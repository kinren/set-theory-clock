package com.paic.arch.interviews.impl;

import com.paic.arch.interviews.TimeConverter;

public class TimeConverterImpl implements TimeConverter {

	@Override
	public String convertTime(String aTime) {
		String[] aTimeArr = aTime.split(":");
		// get hour
		String hourStr = aTimeArr[0];
		// get minute
		String minStr = aTimeArr[1];
		// get second
		String secStr = aTimeArr[2];
		
		// prepare first line
		int sec = Integer.parseInt(secStr);
		StringBuilder sb = new StringBuilder();
		String firstStr = (sec%2 == 0) ? "Y":"O";  		
		sb.append(firstStr).append("\r\n");
		
		// prepare second line
		int hour = Integer.parseInt(hourStr);
		int secondHourNum = hour/5;
		for (int i=0; i<4; i++) {
			if (i<secondHourNum) {
				sb.append("R");
			} else {
				sb.append("O");
			}
		}
		sb.append("\r\n");
		
		// prepare third line
		int thirdHourNum = hour%5;
		for (int i=0; i<4; i++) {
			if (i<thirdHourNum) {
				sb.append("R");
			} else {
				sb.append("O");
			}
		}
		sb.append("\r\n");
		
		// prepare fourth line
		int min = Integer.parseInt(minStr);
		int fourHourNum = min/5;
		for (int i=0; i<11; i++) {
			if (i<fourHourNum) {
				if ((i+1)%3 == 0) {
					sb.append("R");
				} else {
					sb.append("Y");
				}			
			} else {
				sb.append("O");
			}
		}
		sb.append("\r\n");
		
		// prepare fifth line
		int fiveHourNum = min%5;
		System.out.println(fiveHourNum);
		for (int i=0; i<4; i++) {
			if (i<fiveHourNum) {
				sb.append("Y");
			} else {
				sb.append("O");
			}
		}
		
		return sb.toString();
	}
}
